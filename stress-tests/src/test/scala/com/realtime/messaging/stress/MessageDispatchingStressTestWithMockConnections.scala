package com.realtime.messaging.stress

import akka.actor.Props
import akka.stream.javadsl.{Sink, Source}
import com.realtime.messaging.model.{Message, TopicRegistration, Registration}
import com.realtime.messaging.model.registry.TopicRegistry
import com.realtime.messaging.webflow.Publisher
import org.scalatest.FlatSpec

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

class MessageDispatchingStressTestWithMockConnections extends FlatSpec with Infrastructure with StressFixtures {

  val rnd: Random = new Random()

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    val start = System.currentTimeMillis()
    TopicRegistry.buildTopicStore(totalNumberOfTopics, processingGroupCount, confidenceIntervalToPurge)
    val end = System.currentTimeMillis()
    logger.info("TopicRegistry was built in {} secs", (end - start) / 1000)
  }

  private[this] def createRegistrationRequest(uuid: String, clientIdentificationKey: String): Registration = {

    val registrationSet = mutable.HashSet[Int]()

    @tailrec
    def pickNextTopic(registrationSet: mutable.HashSet[Int]): Int = {
      val next = rnd.nextInt(totalNumberOfTopics)
      if (registrationSet.contains(next)) {
        pickNextTopic(registrationSet)
      }
      else {
        registrationSet.add(next)
        next
      }
    }
    Registration(uuid, clientIdentificationKey, Array.fill(topicsPerConnection) {
      TopicRegistration("Topic" + pickNextTopic(registrationSet))
    })
  }

  "As a buyer of Real Time Messaging Server I need to check if the system can handle " + connections + " with " + topicsPerConnection +
    " topics per connection in a universe of " + totalNumberOfTopics + "topics. The system" should "handle all the registration requests in a reasonable time." in {
    val start = System.currentTimeMillis()

    for (i <- 0 until connections) {
      Source.actorPublisher[String](Props(classOf[Publisher], controller, baseSubject + i, baseClientIdentificationKey + i, System.currentTimeMillis() + tokenExpirationInterval, bufferSize))
        .to(Sink.actorSubscriber(Props.create(classOf[MockSubscriber]))).run(materializer)
      if (i % 1000 == 0) logger.info("{} connections established", i)
    }

    for (i <- 0 until connections) {
      controller ! createRegistrationRequest(baseSubject + i, baseClientIdentificationKey + i)
      if (i % 10 == 0) logger.info("{} registrations submitted", i)
    }

    while (TopicRegistry.freeTopicsSize != 0) {
      Thread.sleep(1000)
      logger.info("Remaining topics to be registered {}", TopicRegistry.freeTopicsSize)
    }
    val end = System.currentTimeMillis()
    logger.info("All registrations were completed in {} secs", (end - start) / 1000)
  }


  "As a buyer of Real Time Messaging Server I need to check if the system can handle given its loaded with" + connections + "connections and " + topicsPerConnection +
    " topics per connection in a universe of " + totalNumberOfTopics + "topics, the submission of" + rateOfSubmittedMessagesPerSecond +
    " messages per second. The system" should "dispatch all the messages " +
    "to all subscribers in a reasonable time." in {
    val start = System.currentTimeMillis()
    var ordinal: Long = 0

    val startSendingMessages = System.currentTimeMillis()
    for (i <- 0 until totalNumberOfTopics) {
      dispatcher ! Message(ordinal, "{\"id\":\"someId\", \"payload\":\"o trobiskos tha sas gamhsei olous\", \"status\":\"kariolis\"}", Some("Topic" + i), None)
      ordinal = ordinal + 1
      if (i % rateOfSubmittedMessagesPerSecond == 0) {
        logger.info("Messages dispatched {}", MockSubscriber.CONSUMED_MESSAGES)
        Thread.sleep(1000)
      }
    }
    val endSendingMessages = System.currentTimeMillis()
    logger.info("Messages submitted to for processing in {} secs", (endSendingMessages - startSendingMessages) / 1000)

    var guard = MockSubscriber.CONSUMED_MESSAGES.get()
    var tries = 10
    while (tries > 0) {
      Thread.sleep(500)
      logger.info("Messages dispatched {}", MockSubscriber.CONSUMED_MESSAGES)
      if (guard == MockSubscriber.CONSUMED_MESSAGES.get()) {
        tries = tries - 1
      }
      guard = MockSubscriber.CONSUMED_MESSAGES.get()
    }
    val end = System.currentTimeMillis()
    val runningTime = (end - 5000 - start) / 1000
    val msgPerSec = MockSubscriber.CONSUMED_MESSAGES.get() / runningTime
    logger.info("All messages were dispatched in {} secs", runningTime)
    logger.info("Processing {} messages per sec", msgPerSec)
  }
}
