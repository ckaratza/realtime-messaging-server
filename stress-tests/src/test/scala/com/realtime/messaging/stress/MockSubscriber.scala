package com.realtime.messaging.stress

import java.util.concurrent.atomic.AtomicLong

import akka.stream.actor.ActorSubscriberMessage.OnNext
import akka.stream.actor.{WatermarkRequestStrategy, RequestStrategy, ActorSubscriber}

object MockSubscriber {
  val CONSUMED_MESSAGES = new AtomicLong(0)
}

class MockSubscriber extends ActorSubscriber {
  override protected def requestStrategy: RequestStrategy = WatermarkRequestStrategy.apply(100)

  override def receive: Receive = {
    case onNext: OnNext => MockSubscriber.CONSUMED_MESSAGES.incrementAndGet()
    case x: Any => unhandled(x)
  }
}
