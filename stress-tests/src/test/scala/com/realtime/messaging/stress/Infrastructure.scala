package com.realtime.messaging.stress


import akka.actor.{Props, ActorRef, ActorSystem}
import akka.contrib.throttle.Throttler.{Rate, SetTarget}
import akka.stream.ActorMaterializer
import ch.qos.logback.classic.{Level, LoggerContext}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.realtime.messaging.actor.Controller
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfterAll, Suite, Matchers}
import org.slf4j.LoggerFactory

trait Infrastructure extends Suite with LazyLogging with Matchers with BeforeAndAfterAll {
  val loggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  val rootLogger = loggerContext.getLogger("com.realtime.messaging")
  rootLogger.setLevel(Level.INFO)

  implicit val actorSystem: ActorSystem = ActorSystem.create("rtm" + System.currentTimeMillis(), ConfigFactory.parseString("priority-mailbox {\n" +
    "  mailbox-type = \"com.realtime.messaging.actor.PriorityMailBox\"\n" +
    "}\n" +
    "pinned-dispatcher {\n  executor = \"thread-pool-executor\"\n  type = PinnedDispatcher\n}"))
  implicit val materializer = ActorMaterializer()
  val processingGroupCount = 4
  val purgeInterval = 120
  val confidenceIntervalToPurge: Long = 1000

  protected def ttl: Int = 120000

  val controller: ActorRef = actorSystem.actorOf(Props(classOf[Controller], processingGroupCount, purgeInterval, ttl))
  val dispatcher = actorSystem.actorSelection("/user/messageDispatcher")

  implicit val secret = "everyBodyHasASecret"
  implicit val tokenExpirationInterval = 3600000L
  implicit val bufferSize = 1000
  implicit val objectMapper = new ObjectMapper
  objectMapper.registerModule(new DefaultScalaModule)
}
