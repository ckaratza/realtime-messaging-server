package com.realtime.messaging.stress

trait StressFixtures {

  val connections = 50000
  val topicsPerConnection = 50
  val totalNumberOfTopics = 1000

  val rateOfSubmittedMessagesPerSecond = 250

  val baseClientIdentificationKey = "key"
  val baseSubject = "baseSubject"

}
