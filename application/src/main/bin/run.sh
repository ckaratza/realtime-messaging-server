#!/bin/sh

APPLICATION=realtime.messaging

APPLICATION_HOME=$( cd $(dirname $0)/.. && pwd )
APPLICATION_CFG_DIR=$APPLICATION_HOME/config
APPLICATION_LOGS=$APPLICATION_HOME/logs


JVM_ARGS="-ea -Xms8G -XX:+UseConcMarkSweepGC \
 -Dfile.encoding=UTF-8 \
 -Dcom.sun.management.jmxremote.port=19000\
 -Dcom.sun.management.jmxremote.authenticate=false\
 -Dcom.sun.management.jmxremote.ssl=false"


    JVM_ARGS="$JVM_ARGS\
 -Dlogback.configurationFile=${APPLICATION_CFG_DIR}/logback.xml\
 -Dlogging.config=file:${APPLICATION_CFG_DIR}/logback.xml"


CLASSPATH=$(
    find $APPLICATION_HOME/lib -name '*.jar' \
        | perl -nle 'our @jar; push @jar, $_; END { print join q{:}, @jar }'
)

MAIN_CLASS=com.$APPLICATION.application.Application

java_pid=

stop () {
    [ "$java_pid" ] && kill $java_pid
    exit
}

bounce () {
    [ "$java_pid" ] && kill $java_pid
    trap bounce HUP
}

trap stop   INT TERM
trap bounce HUP

cd $APPLICATION_HOME

while : ; do

    ( java -classpath $CLASSPATH $JVM_ARGS -Dmode=production -DLOG_PATH=$APPLICATION_LOGS/ -DappConfig=$APPLICATION_CFG_DIR/ $MAIN_CLASS) &
    java_pid=$!

    wait $java_pid

done