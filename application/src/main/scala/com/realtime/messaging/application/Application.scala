package com.realtime.messaging.application

import java.io.File

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.cluster.client.ClusterClientReceptionist
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.realtime.messaging.actor.Controller
import com.realtime.messaging.model.registry.TopicRegistry
import com.realtime.messaging.receptionist.MessageReceptionist
import com.realtime.messaging.server.WebSocketServer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future
import scala.io.StdIn

object Application extends App with WebSocketServer with LazyLogging {

  val config = System.getProperty("appConfig", "N/A") match {
    case "N/A" => ConfigFactory.load()
    case configPath: String => ConfigFactory.parseFile(new File(configPath + "application.conf"))
  }
  implicit val system = ActorSystem("rtm", config)
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val tokenExpirationInterval: Long = config.getLong("configuration.tokenExpirationInterval")
  implicit val secret: String = config.getString("configuration.secret")
  implicit val bufferSize: Int = config.getInt("configuration.bufferSize")
  implicit val objectMapper = new ObjectMapper
  objectMapper.registerModule(new DefaultScalaModule)
  TopicRegistry.buildTopicStore(config.getInt("configuration.universeSize"), config.getInt("configuration.processingGroupCount"),
    config.getLong("configuration.confidenceIntervalToPurge"))

  val controller: ActorRef = system.actorOf(Props(classOf[Controller], config.getInt("configuration.processingGroupCount"),
    config.getInt("configuration.purgeInterval"), config.getInt("configuration.ttl")))

  val messagingReceptionist = system.actorOf(Props(classOf[MessageReceptionist], system.actorSelection("/user/messageDispatcher")), "messagingService")
  ClusterClientReceptionist(system).registerService(messagingReceptionist)

  val (host, port) = (config.getString("configuration.host"), config.getInt("configuration.port"))
  val bindingFuture: Future[ServerBinding] = Http().bindAndHandle(wsRoute, host, port)

  bindingFuture.onFailure {
    case ex: Exception =>
      logger.error("Failed to bind to {}:{} because of {}", host, port + "", ex.getMessage)
      terminate()
  }

  logger.info(s"Real-Time Messaging Server online at http://{}:{}/rtm/{token}/{key}", host, port + "")
  System.getProperty("mode", "N/A") match {
    case "production" => logger.info("Running in production mode")
    case _: Any =>
      logger.info("Press RETURN to stop...")
      StdIn.readLine()
      bindingFuture.flatMap(_.unbind()).onComplete(_ => terminate())
  }

  def terminate() = {
    system.terminate().foreach { _ =>
      logger.info("Actor system was shut down")
    }
  }
}


