package com.realtime.messaging.receptionist

import akka.actor.{ActorSelection, Actor}
import com.realtime.messaging.model.Message
import com.typesafe.scalalogging.LazyLogging

class MessageReceptionist(messageDispatcher: ActorSelection) extends Actor with LazyLogging {

  override def receive: Receive = {
    case msg: Message => logger.info("Receptionist received message {}", msg); messageDispatcher ! msg
    case x: Any => logger.warn("Unhandled Message {}", x); unhandled(x)
  }
}
