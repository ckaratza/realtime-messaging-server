package com.realtime.messaging.receptionist

import akka.actor.{ActorSystem, ActorPath}
import akka.cluster.client.{ClusterClient, ClusterClientSettings}
import com.realtime.messaging.fixture.BasicFixtures
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging

import scala.io.StdIn
import scala.util.control.Breaks

object ReceptionistClientExample extends App with LazyLogging with BasicFixtures {

  val system: ActorSystem = ActorSystem.create("rtmPusher", ConfigFactory.parseString(
    "akka.extensions = [\"akka.cluster.client.ClusterClientReceptionist\"]\nakka.actor.provider=\"akka.cluster.ClusterActorRefProvider\"" +
      "\nakka.remote.netty.tcp.hostname=\"localhost\"\nakka.remote.netty.tcp.port=6666" +
      "\nakka.cluster.seed-nodes = [\"akka.tcp://rtmPusher@localhost:6666\"]"))

  val initialContacts = Set(ActorPath.fromString("akka.tcp://rtm@localhost:19002/system/receptionist"))
  val settings = ClusterClientSettings(system).withInitialContacts(initialContacts)

  val messageSender = system.actorOf(ClusterClient.props(ClusterClientSettings(system).withInitialContacts(initialContacts)), "messageSender")
  val loop = new Breaks
  loop.breakable {
    while (sendMessageStack.nonEmpty) {
      logger.info("Press RETURN to send new message or q to stop...")
      StdIn.readLine() match {
        case "q" => loop.break()
        case _: String => messageSender ! ClusterClient.Send("/user/messagingService", sendMessageStack.pop(), localAffinity = true)
      }
    }
  }
  system.shutdown()
}
