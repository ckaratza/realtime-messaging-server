package com.realtime.messaging.fixture

import com.realtime.messaging.model.Message

import scala.collection.mutable

trait BasicFixtures {
  val subject = "ckaratza"
  val clientIdentificationKey = "key"
  val registrationTopic = "Topic0"


  val sendMessageStack = mutable.Stack[Message]()
  sendMessageStack.push(Message(1, "Hello Mr Trabarifa!", Some(registrationTopic), None))
  sendMessageStack.push(Message(0, "Hello Mr Trobisko!", Some(registrationTopic), None))

  val incomingMessages = Array("Hello Mr Trobisko!", "Hello Mr Trabarifa!")
}
