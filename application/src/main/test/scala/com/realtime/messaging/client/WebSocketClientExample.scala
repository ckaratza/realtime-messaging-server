package com.realtime.messaging.client

import java.util.concurrent.atomic.AtomicInteger

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.realtime.messaging.common.security.TokenHandler
import com.realtime.messaging.fixture.BasicFixtures
import com.realtime.messaging.model.{TopicRegistration, Registration}
import com.typesafe.scalalogging.LazyLogging
import org.jfarcand.wcs.{WebSocket, TextListener}

import scala.io.StdIn

object WebSocketClientExample extends App with LazyLogging with BasicFixtures {

  implicit val tokenExpirationInterval: Long = 1000000
  implicit val secret: String = "everyBodyHasASecret"
  implicit val bufferSize: Int = 100
  implicit val objectMapper = new ObjectMapper
  objectMapper.registerModule(new DefaultScalaModule)
  val connections = 50
  val deliveredMessages = new AtomicInteger(0)


  def connect(subject: String): WebSocket = {
    val token = TokenHandler.createTokenForSubject(subject, createExpiredToken = false)
    WebSocket().open("ws://localhost:19001/rtm/" + token.right.get + "/" + clientIdentificationKey)
      .send(objectMapper.writeValueAsString(Registration("", "", Array(TopicRegistration(registrationTopic)))))
  }

  def registerReceivingMessageSequence(webSocket: WebSocket): WebSocket = {
    webSocket.listener(new TextListener {
      override def onMessage(message: String): Unit = {
        logger.info("Received message [{}] with number {}", message, deliveredMessages.incrementAndGet() + "")
      }
    })
  }

  val webSocketConnections = Array.tabulate(connections) { ordinal =>
    logger.info("Creating Connection {}", ordinal)
    registerReceivingMessageSequence(connect(subject + ordinal))
  }
  logger.info("Press RETURN to stop...")
  StdIn.readLine()
  webSocketConnections.foreach(connection => connection.close)

  assert(deliveredMessages.get() == connections * incomingMessages.length)
  System.exit(0)
}