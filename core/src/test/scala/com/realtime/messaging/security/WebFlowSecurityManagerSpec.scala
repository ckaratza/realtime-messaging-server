package com.realtime.messaging.security

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.realtime.messaging.common.exception.FailReason
import com.realtime.messaging.common.security.TokenHandler
import com.realtime.messaging.model._
import org.scalatest.{FlatSpec, Matchers}

class WebFlowSecurityManagerSpec extends FlatSpec with Matchers {

  implicit val secret = "everyBodyHasASecret"
  implicit val tokenExpirationInterval = 3000L
  implicit val objectMapper = new ObjectMapper
  objectMapper.registerModule(new DefaultScalaModule)
  val clientIdentificationKeyFixture = "key1"
  val subjectFixture = "ckaratza"
  val badTokenFixture = "badToken"
  val registrationFixture = Registration(subjectFixture, clientIdentificationKeyFixture, Array(TopicRegistration("topicA", 123553521L)))
  val unRegistrationFixture = UnRegistration(subjectFixture, clientIdentificationKeyFixture, Array("TopicA", "TopicB"))
  val resumeFlowDataFixture = ResumeFlowData(ConsumerData(subjectFixture, clientIdentificationKeyFixture, System.currentTimeMillis))

  private[this] def processJson[T <: Protocol](json: String, uuid: String, clientIdentificationKey: String, assertionFunction: (Option[T]) => Unit): Unit = {
    val deSerializedValue = WebFlowSecurityManager.transformWebFlowInboundData(json, uuid, clientIdentificationKey)
    assertionFunction(deSerializedValue.asInstanceOf[Option[T]])
  }

  "Real-time Messaging Server receives a connection request from a client with subject name" + subjectFixture + ", a valid token and clientIdentificationKey" + clientIdentificationKeyFixture +
    ". WebFlowSecurityManager" should "process the input data and produce a valid ConsumerData object for further use by the system" in {
    val token = TokenHandler.createTokenForSubject(subjectFixture).right.get
    val consumerData = WebFlowSecurityManager.transformWebFlowSecurityDataToConsumerData(token, clientIdentificationKeyFixture).right.get
    consumerData.uuid should be(subjectFixture)
    consumerData.clientIdentificationKey should be(clientIdentificationKeyFixture)
  }

  "Real-time Messaging Server receives a connection request from a client with subject name" + subjectFixture + ", an expired token and clientIdentificationKey" + clientIdentificationKeyFixture +
    ". WebFlowSecurityManager" should "process the input data and reject the connection request" in {
    val token = TokenHandler.createTokenForSubject(subjectFixture, true).right.get
    val failResult = WebFlowSecurityManager.transformWebFlowSecurityDataToConsumerData(token, clientIdentificationKeyFixture).left.get
    failResult.failReason should be(FailReason.SESSION_EXPIRATION)
  }

  "Real-time Messaging Server receives a connection request from a client with subject name" + subjectFixture + ", a badly formed token and clientIdentificationKey" + clientIdentificationKeyFixture +
    ". WebFlowSecurityManager" should "process the input data and reject the connection request" in {
    val failResult = WebFlowSecurityManager.transformWebFlowSecurityDataToConsumerData(badTokenFixture, clientIdentificationKeyFixture).left.get
    failResult.failReason should be(FailReason.UNAUTHENTICATED_CALL)
  }

  "Real-time Messaging Server receives an inbound message as json string of type WebFlowSecurityData with refresh session intention. WebFlowSecurityManager" should
    "deserialize the input data and create a valid ResumeFlowData Object" in {
    val token = TokenHandler.createTokenForSubject(subjectFixture).right.get
    val webFlowSecurityData = WebFlowSecurityData(token, clientIdentificationKeyFixture)
    processJson[ResumeFlowData](objectMapper.writeValueAsString(webFlowSecurityData), subjectFixture, clientIdentificationKeyFixture, (deSerializedValue) => {
      deSerializedValue.get.consumerData.uuid should be(subjectFixture)
      deSerializedValue.get.consumerData.clientIdentificationKey should be(clientIdentificationKeyFixture)
    })
  }

  "Real-time Messaging Server receives an inbound message as json string of type WebFlowSecurityData with logout intention. WebFlowSecurityManager" should
    "deserialize the input data and create a valid Logout Object" in {
    val token = TokenHandler.createTokenForSubject(subjectFixture).right.get
    val webFlowSecurityData = WebFlowSecurityData(token, clientIdentificationKeyFixture, logout = true)
    processJson[Logout](objectMapper.writeValueAsString(webFlowSecurityData),  subjectFixture, clientIdentificationKeyFixture, (deSerializedValue) => {
      deSerializedValue.get.uuid should be(subjectFixture)
      deSerializedValue.get.clientIdentificationKey should be(clientIdentificationKeyFixture)
    })
  }

  "Real-time Messaging Server receives an inbound message as json string of type Registration. WebFlowSecurityManager" should
    "deserialize the input data and create a valid Registration Object" in {
    processJson[Registration](objectMapper.writeValueAsString(registrationFixture),  subjectFixture, clientIdentificationKeyFixture,(deSerializedValue) => {
      deSerializedValue.get.topics(0).id should be(registrationFixture.topics(0).id)
      deSerializedValue.get.topics(0).fromOrdinal should be(registrationFixture.topics(0).fromOrdinal)
   })
  }

  "Real-time Messaging Server receives an inbound message as json string of type UnRegistration. WebFlowSecurityManager" should
    "deserialize the input data and create a valid UnRegistration Object" in {
    processJson[UnRegistration](objectMapper.writeValueAsString(unRegistrationFixture), subjectFixture, clientIdentificationKeyFixture, (deSerializedValue) => {
      deSerializedValue.get.uuid should be(subjectFixture)
      deSerializedValue.get.topicIds should be(List("TopicA", "TopicB"))
   })
  }
}
