package com.realtime.messaging.model.registry

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.TestProbe
import com.realtime.messaging.model.ConsumerData
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

class PublisherRegistrySpec extends FlatSpec with Matchers with BeforeAndAfterAll with LazyLogging {

  implicit val actorSystem: ActorSystem = ActorSystem.create()
  implicit val executionContext = actorSystem.dispatcher
  val actorRef: ActorRef = TestProbe.apply().ref
  val uuidCommonFixture = "ckaratza"
  val consumerDataFixture1 = ConsumerData(uuidCommonFixture, "key1", 123L)
  val consumerDataFixture1_1 = ConsumerData(uuidCommonFixture, "key1", 123456L)
  val consumerDataFixture2 = ConsumerData(uuidCommonFixture, "key2", 1234L)
  val refreshedExpirationTimeForFixture2 = 12346L
  val consumerDataFixture3 = ConsumerData(uuidCommonFixture, "key3", 12345L)

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    PublisherRegistry.clear()
  }

  override protected def afterAll(): Unit = {
    actorSystem.terminate().foreach { _ =>
      logger.info("Actor system was shut down")
    }
  }

  "A valid publisherRegistry multi-map implementation" should "successfully store multiple bindings with the same uuid and different clientIdentificationKey and successfully retrieve them" in {

    PublisherRegistry.addNewPublisher(consumerDataFixture1.uuid, consumerDataFixture1.clientIdentificationKey, consumerDataFixture1.expiresAt, actorRef)
    PublisherRegistry.addNewPublisher(consumerDataFixture2.uuid, consumerDataFixture2.clientIdentificationKey, consumerDataFixture2.expiresAt, actorRef)
    PublisherRegistry.addNewPublisher(consumerDataFixture3.uuid, consumerDataFixture3.clientIdentificationKey, consumerDataFixture3.expiresAt, actorRef)

    PublisherRegistry.findPublishersForUUID(uuidCommonFixture).get.size should be(3)
  }

  "A valid publisherRegistry multi-map implementation" should "successfully override a binding with identical uuid and clientIdentificationKey" in {
    PublisherRegistry.addNewPublisher(consumerDataFixture1_1.uuid, consumerDataFixture1_1.clientIdentificationKey, consumerDataFixture1_1.expiresAt, actorRef)
    PublisherRegistry.findPublishersForUUID(uuidCommonFixture).get.size should be(3)
    PublisherRegistry.findPublishersForUUID(uuidCommonFixture).get.find(entry =>
      entry.clientIdentificationKey == consumerDataFixture1.clientIdentificationKey).get.expiresAt should be(consumerDataFixture1_1.expiresAt)
  }

  "A valid publisherRegistry multi-map implementation" should "successfully retrieve a binding with a specific clientIdentificationKey" in {
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(uuidCommonFixture, consumerDataFixture3.clientIdentificationKey).get should not be None
  }

  "A valid publisherRegistry multi-map implementation" should "successfully remove the correct binding with a specific clientIdentificationKey" in {
    PublisherRegistry.removePublisher(uuidCommonFixture, consumerDataFixture3.clientIdentificationKey)
    PublisherRegistry.findPublishersForUUID(uuidCommonFixture).get.size should be(2)
    PublisherRegistry.findPublishersForUUID(uuidCommonFixture).get.count(p => p.clientIdentificationKey == consumerDataFixture3.clientIdentificationKey) should be(0)
  }

  "A valid publisherRegistry multi-map implementation" should "successfully update the correct binding with a specific clientIdentificationKey with a new expiration time" in {
    PublisherRegistry.refreshExpirationTimeForUuidAndClientIdentificationKey(uuidCommonFixture, consumerDataFixture2.clientIdentificationKey, refreshedExpirationTimeForFixture2)
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(uuidCommonFixture, consumerDataFixture2.clientIdentificationKey).get.expiresAt should be(refreshedExpirationTimeForFixture2)
  }
}
