package com.realtime.messaging.server

import com.realtime.messaging.model._

trait BasicFixtures {
  val subjectFixture1 = "ckaratza"
  val clientIdentificationKeyFixture1 = "myKey"

  val subjectFixture2 = "chrisvonkaratzas"
  val clientIdentificationKeyFixture2 = "myKey1"

  val messageFixture1 = Message(0, "Hello ckaratza", None, Option(Array(subjectFixture1)))
  val messageFixture2 = Message(1, "How are you Sir", None, Option(Array(subjectFixture1)))

  val messageFixture3 = Message(0, "Hello ckaratza", None, Option(Array(subjectFixture1, subjectFixture2)))
  val messageFixture4 = Message(1, "How are you Sir", None, Option(Array(subjectFixture1, subjectFixture2)))

  val messageFixture1ForTopic = Message(0, "Hello ckaratza", Some("Topic0"), None)
  val messageFixture2ForTopic = Message(1, "How are you Sir", Some("Topic0"), None)

  val messageFixture3ForTopic = Message(2, "Hello Trobisko", Some("Topic0"), None)
  val messageFixture4ForTopicWithHighOrdinal = Message(10, "Hello Trobisko", Some("Topic0"), None)


  val registrationFixture = Registration(subjectFixture1, clientIdentificationKeyFixture1, Array(TopicRegistration("Topic0")))
  val unRegistrationFixture = UnRegistration(subjectFixture1, clientIdentificationKeyFixture1, Array("Topic0"))

  val registrationFixtureWithHighOdinal = Registration(subjectFixture1, clientIdentificationKeyFixture1, Array(TopicRegistration("Topic0", 10)))

  val topicToBePurged = Array.tabulate(10) { ordinal => "Topic" + ordinal }
  val messagesForToBePurgedTopics = Array.tabulate(10) { ordinal => Message(ordinal, "Hello Mr Trobisko" + ordinal, Some(topicToBePurged(ordinal)), None) }

  val registrationForToBePurgedTopics1 = Registration(subjectFixture1, clientIdentificationKeyFixture1,
    Array.tabulate(5) { ordinal => TopicRegistration(topicToBePurged(ordinal)) })
  val registrationForToBePurgedTopics2 = Registration(subjectFixture2, clientIdentificationKeyFixture2,
    Array.tabulate(5) { ordinal => TopicRegistration(topicToBePurged(9 - ordinal)) })

  val unRegistrationForToBePurgedTopics = UnRegistration("", "", topicToBePurged)
}
