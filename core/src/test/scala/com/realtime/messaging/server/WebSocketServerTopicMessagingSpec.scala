package com.realtime.messaging.server

import akka.actor.ActorSelection
import akka.http.scaladsl.testkit.WSProbe
import com.realtime.messaging.model.registry.TopicRegistry
import org.scalatest.time.{Milliseconds, Span}
import org.scalatest.{BeforeAndAfterAll, FlatSpec}

class WebSocketServerTopicMessagingSpec extends FlatSpec with WebSocketServer with Infrastructure with BasicFixtures {

  override implicit val tokenExpirationInterval: Long = 6000

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    TopicRegistry.buildTopicStore(1, processingGroupCount, confidenceIntervalToPurge)
  }

  "Real-time messaging server web-socket channel receives a valid connection request from subject " + subjectFixture1 +
    ". The same connection issues a registration for a specific topic. After that messages are dispatched for the specific topic. Real-time server " should
    "handle the registration successfully and dispatch subsequent messages originating from the internal system to subject recipient because of its topic registration. " +
      "After receiving the messages a logout must be handled properly" in {
    val (wsClient, token, path) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    WS(path, wsClient.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true
        wsClient.sendMessage(objectMapper.writeValueAsString(registrationFixture))
        waitUntilTimeout1[ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(dispatcher) (
          (dispatcher) => {
            dispatcher ! messageFixture1ForTopic
            dispatcher ! messageFixture2ForTopic
            }
        )
        wsClient.expectMessage(messageFixture1ForTopic.payload)
        wsClient.expectMessage(messageFixture2ForTopic.payload)
        logout(wsClient, token.right.get, subjectFixture1, clientIdentificationKeyFixture1)
      }
  }

  "Real-time messaging server web-socket channel receives a valid connection request from subject " + subjectFixture1 +
    ". The same connection issues a registration for a specific topic. After that messages are dispatched for the specific topic. After that client issues" +
    " an un-registration request from the specific topic. Real-time server " should
    "handle the registration successfully and dispatch subsequent messages originating from the internal system to subject recipient because of its topic registration. " +
      "After the un-registration the client must not receive any messages for the specific topic. " +
      "After receiving the messages a logout must be handled properly" in {
    val (wsClient, token, path) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    WS(path, wsClient.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true
        wsClient.sendMessage(objectMapper.writeValueAsString(registrationFixture))
        waitUntilTimeout1[ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(dispatcher) (
          (dispatcher) => {
            dispatcher ! messageFixture1ForTopic
            dispatcher ! messageFixture2ForTopic
            }
        )
        wsClient.expectMessage(messageFixture1ForTopic.payload)
        wsClient.expectMessage(messageFixture2ForTopic.payload)
        wsClient.sendMessage(objectMapper.writeValueAsString(unRegistrationFixture))
        waitUntilTimeout2[WSProbe, ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(wsClient, dispatcher)(
        (wsClient, dispatcher) => {
          dispatcher ! messageFixture1ForTopic
          wsClient.expectNoMessage()
        })
        logout(wsClient, token.right.get, subjectFixture1, clientIdentificationKeyFixture1)
      }
  }

}
