package com.realtime.messaging.server

import akka.actor.ActorSelection
import akka.http.scaladsl.testkit.WSProbe
import com.realtime.messaging.model.registry.TopicRegistry
import org.scalatest.FlatSpec
import org.scalatest.time.{Seconds, Milliseconds, Span}

class WebSocketServerPurgeSpec extends FlatSpec with WebSocketServer with Infrastructure with BasicFixtures {

  override implicit val tokenExpirationInterval: Long = 600000

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    TopicRegistry.buildTopicStore(10, processingGroupCount, confidenceIntervalToPurge)
  }

  "Real-Time Messaging Server receives registration requests for 10 topics from 2 subjects. In the meanwhile the 2 subjects have left the system. " +
    "Real-Time Messaging Server Purging Functionality" should "eventually decommission the 10 topic slots because of no subscribers" in {

    val (wsClient1, token1, path1) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    val (wsClient2, token2, path2) = createTupleForLogin(subjectFixture2, clientIdentificationKeyFixture2)

    eventually(timeout(Span(toleranceInterval, Milliseconds)))(
      WS(path1, wsClient1.flow) ~> wsRoute ~>
        check {
          isWebSocketUpgrade shouldEqual true
          wsClient1.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics1))
          wsClient1.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics2))
        })

    eventually(timeout(Span(toleranceInterval, Milliseconds)))(
      WS(path2, wsClient2.flow) ~> wsRoute ~>
        check {
          isWebSocketUpgrade shouldEqual true
          wsClient2.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics1))
          wsClient2.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics2))
        })

    waitUntilTimeout1[ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(dispatcher) (
          (dispatcher) => {
             for (i <- 0 until 10) {
               dispatcher ! messagesForToBePurgedTopics(i)
              }
            })

    waitUntilTimeout2[WSProbe, WSProbe](timeout(Span(toleranceInterval, Milliseconds)))(wsClient1, wsClient2)(
    (wsClient1, wsClient2) => {
      for (i <- 0 until 10) {
        wsClient1.expectMessage(messagesForToBePurgedTopics(i).payload)
        wsClient2.expectMessage(messagesForToBePurgedTopics(i).payload)
      }
      }
    )
    logout(wsClient1, token1.right.get, subjectFixture1, clientIdentificationKeyFixture1)
    logout(wsClient2, token2.right.get, subjectFixture2, clientIdentificationKeyFixture2)

    waitUntilTimeout0(timeout(Span(2 * purgeInterval, Seconds))) {
      TopicRegistry.freeTopicsSize should be(10)
    }
  }

  "Real-Time Messaging Server receives registration requests for 10 topics from 2 subjects. In the meanwhile the 2 subjects un-register from these topics. " +
    "Real-Time Messaging Server Purging Functionality" should "eventually decommission the 10 topic slots because of no subscribers." in {

    val (wsClient1, token1, path1) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    val (wsClient2, token2, path2) = createTupleForLogin(subjectFixture2, clientIdentificationKeyFixture2)

    eventually(timeout(Span(toleranceInterval, Milliseconds)))(
      WS(path1, wsClient1.flow) ~> wsRoute ~>
        check {
          isWebSocketUpgrade shouldEqual true
          wsClient1.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics1))
          wsClient1.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics2))
        })

    eventually(timeout(Span(toleranceInterval, Milliseconds)))(
      WS(path2, wsClient2.flow) ~> wsRoute ~>
        check {
          isWebSocketUpgrade shouldEqual true
          wsClient2.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics1))
          wsClient2.sendMessage(objectMapper.writeValueAsString(registrationForToBePurgedTopics2))
        })

    waitUntilTimeout1[ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(dispatcher) (
          (dispatcher) => {
             for (i <- 0 until 10) {
               dispatcher ! messagesForToBePurgedTopics(i)
              }
            })

    for (i <- 0 until 10) {
      wsClient1.expectMessage(messagesForToBePurgedTopics(i).payload)
      wsClient2.expectMessage(messagesForToBePurgedTopics(i).payload)
    }

    wsClient1.sendMessage(objectMapper.writeValueAsString(unRegistrationForToBePurgedTopics))
    wsClient2.sendMessage(objectMapper.writeValueAsString(unRegistrationForToBePurgedTopics))

    waitUntilTimeout0(timeout(Span(2 * purgeInterval, Seconds))) {
      TopicRegistry.freeTopicsSize should be(10)
    }

    logout(wsClient1, token1.right.get, subjectFixture1, clientIdentificationKeyFixture1)
    logout(wsClient2, token2.right.get, subjectFixture2, clientIdentificationKeyFixture2)
  }
}
