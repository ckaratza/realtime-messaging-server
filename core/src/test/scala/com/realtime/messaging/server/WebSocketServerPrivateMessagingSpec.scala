package com.realtime.messaging.server

import akka.actor.ActorSelection
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.WSProbe
import com.realtime.messaging.common.security.TokenHandler
import com.realtime.messaging.model.{RefreshSession, WebFlowSecurityData}
import org.scalatest.FlatSpec
import org.scalatest.time.{Milliseconds, Span}

class WebSocketServerPrivateMessagingSpec extends FlatSpec with WebSocketServer with Infrastructure with BasicFixtures {

  "Real-time messaging server web-socket channel receives connection request with expired token from subject " + subjectFixture1 + ". WebSocketServer" should
    "reject the request as unauthorized" in {
    val (wsClient, _, path) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1, expiredToken = true)
    WS(path, wsClient.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual false
        status should be(StatusCodes.Unauthorized)
      }
  }

  "Real-time messaging server web-socket channel receives a valid connection request from subject " + subjectFixture1 + ". WebSocketServer" should
    "handle the request successfully and dispatch subsequent messages originating from the internal system to subject recipient. " +
      "After receiving the messages a logout must be handled properly" in {
    val (wsClient, token, path) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    WS(path, wsClient.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true

        waitUntilTimeout2[WSProbe, ActorSelection](timeout(Span(joinClientInterval, Milliseconds)))(wsClient, dispatcher)(
          (wsClient, dispatcher) => {
            dispatcher ! messageFixture1
            dispatcher ! messageFixture2
            wsClient.expectMessage(messageFixture1.payload)
            wsClient.expectMessage(messageFixture2.payload)
          })

        logout(wsClient, token.right.get, subjectFixture1, clientIdentificationKeyFixture1)
      }
  }

  "Real-time messaging server web-socket channel receives a valid connection request from subject " + subjectFixture1 + ". WebSocketServer" should
    "handle the request successfully and dispatch subsequent messages originating from the internal system to subject recipient. " +
      "After the token expiration interval is exhausted the internal system must ask for a new token from the web-socket client. " +
      "In the meanwhile all subsequent messages from the internal system should not be dispatched until the refreshed token is submitted." in {
    val (wsClient, token, path) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    WS(path, wsClient.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true

        waitUntilTimeout2[WSProbe, ActorSelection](timeout(Span(joinClientInterval, Milliseconds)))(wsClient, dispatcher)(
          (wsClient, dispatcher) => {
            dispatcher ! messageFixture1
            wsClient.expectMessage(messageFixture1.payload)
          })

        waitUntilTimeout2[WSProbe, ActorSelection](timeout(Span(tokenExpirationInterval, Milliseconds)))(wsClient, dispatcher)(
        (wsClient, dispatcher) => {
          dispatcher ! messageFixture2
          wsClient.expectMessage(RefreshSession.refresh)
          val newToken = TokenHandler.createTokenForSubject(subjectFixture1)
          wsClient.sendMessage(objectMapper.writeValueAsString(WebFlowSecurityData(newToken.right.get, clientIdentificationKeyFixture1)))
          wsClient.expectMessage(messageFixture2.payload)
          logout(wsClient, newToken.right.get, subjectFixture1, clientIdentificationKeyFixture1)
        })
      }
  }

  "Real-time messaging server web-socket channel receives 2 valid connection request from subjects " + subjectFixture1 + "," + subjectFixture2 + ". WebSocketServer" should
    "handle the request successfully and dispatch subsequent messages originating from the internal system to both 2 subject recipients. " in {
    val (wsClient1, token1, path1) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    val (wsClient2, token2, path2) = createTupleForLogin(subjectFixture2, clientIdentificationKeyFixture2)

    eventually(timeout(Span(toleranceInterval, Milliseconds)))(
      WS(path1, wsClient1.flow) ~> wsRoute ~>
        check {
          isWebSocketUpgrade shouldEqual true
        })

    eventually(timeout(Span(toleranceInterval, Milliseconds)))(
      WS(path2, wsClient2.flow) ~> wsRoute ~>
        check {
          isWebSocketUpgrade shouldEqual true
        })

    eventually(timeout(Span(toleranceInterval, Milliseconds))) {
      dispatcher ! messageFixture3
      dispatcher ! messageFixture4
      wsClient1.expectMessage(messageFixture3.payload)
      wsClient1.expectMessage(messageFixture4.payload)
      wsClient2.expectMessage(messageFixture3.payload)
      wsClient2.expectMessage(messageFixture4.payload)
    }
  }
}
