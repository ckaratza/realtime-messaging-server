package com.realtime.messaging.server

import akka.actor.ActorSelection
import akka.http.scaladsl.testkit.WSProbe
import com.realtime.messaging.model.registry.TopicRegistry
import org.scalatest.time.{Milliseconds, Span}
import org.scalatest.FlatSpec

class WebSocketServerTopicContentDeliverySpec extends FlatSpec with WebSocketServer with Infrastructure with BasicFixtures {

  override implicit val tokenExpirationInterval: Long = 60000

  override def ttl: Int = 10000

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    TopicRegistry.buildTopicStore(1, processingGroupCount, confidenceIntervalToPurge)
  }

  "Real-Time Messaging Server accepts 2 valid subject connections and a registration to a specific topic initially from subject 1. " +
    "Messages are sent to the topic. After ttl interval subject 2 registers also to topic. Real-Time Messaging Server" should "not deliver any old messages " +
    "to subject 2 because they are expired" in {

    val (wsClient1, token1, path1) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    val (wsClient2, token2, path2) = createTupleForLogin(subjectFixture2, clientIdentificationKeyFixture2)

    WS(path1, wsClient1.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true
        wsClient1.sendMessage(objectMapper.writeValueAsString(registrationFixture))
        waitUntilTimeout1[ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(dispatcher) (
          (dispatcher) => {
            dispatcher ! messageFixture1ForTopic
            dispatcher ! messageFixture2ForTopic
            }
        )
        wsClient1.expectMessage(messageFixture1ForTopic.payload)
        wsClient1.expectMessage(messageFixture2ForTopic.payload)
      }

    Thread.sleep(ttl)

    WS(path2, wsClient2.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true
        wsClient2.sendMessage(objectMapper.writeValueAsString(registrationFixture))
        dispatcher ! messageFixture3ForTopic
        wsClient2.expectMessage(messageFixture3ForTopic.payload)
      }
    logout(wsClient1, token1.right.get, subjectFixture1, clientIdentificationKeyFixture1)
    logout(wsClient2, token2.right.get, subjectFixture2, clientIdentificationKeyFixture2)
  }


  "Real-Time Messaging Server accepts 1 valid subject connections and a registration to a specific topic from with a relative high ordinal. " +
    "Some messages are sent to the topic with ordinal less than that of the registration and 1 with the desired ordinal. Real-Time Messaging Server" should "not deliver any messages " +
    "to subject because their ordinal is low and only dispatch the 1 with the high ordinal." in {
    val (wsClient1, token1, path1) = createTupleForLogin(subjectFixture1, clientIdentificationKeyFixture1)
    WS(path1, wsClient1.flow) ~> wsRoute ~>
      check {
        isWebSocketUpgrade shouldEqual true
        waitUntilTimeout2[WSProbe, ActorSelection](timeout(Span(joinClientInterval, Milliseconds)))(wsClient1, dispatcher)(
          (wsClient1, dispatcher) => {
        wsClient1.sendMessage(objectMapper.writeValueAsString(registrationFixtureWithHighOdinal))
        })
        waitUntilTimeout1[ActorSelection](timeout(Span(toleranceInterval, Milliseconds)))(dispatcher) (
          (dispatcher) => {
            dispatcher ! messageFixture1ForTopic
            dispatcher ! messageFixture2ForTopic
            dispatcher ! messageFixture4ForTopicWithHighOrdinal
            }
        )
        wsClient1.expectMessage(messageFixture4ForTopicWithHighOrdinal.payload)
      }
    logout(wsClient1, token1.right.get, subjectFixture1, clientIdentificationKeyFixture1)
  }

}
