package com.realtime.messaging.server

import java.util.concurrent.{Callable, Executors}

import akka.actor.{PoisonPill, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.testkit.{ScalatestRouteTest, WSProbe}
import ch.qos.logback.classic.{Level, LoggerContext}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.realtime.messaging.actor.Controller
import com.realtime.messaging.common.security.TokenHandler
import com.realtime.messaging.model.WebFlowSecurityData
import com.realtime.messaging.model.registry.PublisherRegistry
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.concurrent.{Eventually, JavaFutures}
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.time.{Milliseconds, Seconds, Span}
import org.scalatest._
import org.slf4j.LoggerFactory

trait Infrastructure extends Suite with ScalatestRouteTest with Matchers with JavaFutures with Eventually with BeforeAndAfterAll with LazyLogging {

  val loggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  val rootLogger = loggerContext.getLogger("com.realtime.messaging")
  rootLogger.setLevel(Level.DEBUG)

  implicit val actorSystem: ActorSystem = ActorSystem.create("rtm" + System.currentTimeMillis(), ConfigFactory.parseString("priority-mailbox {\n" +
    "  mailbox-type = \"com.realtime.messaging.actor.PriorityMailBox\"\n" +
    "}\n" +
    "pinned-dispatcher {\n  executor = \"thread-pool-executor\"\n  type = PinnedDispatcher\n}"))
  val processingGroupCount = 4
  val purgeInterval = 10
  protected  def ttl: Int = 30000
  val controller: ActorRef = actorSystem.actorOf(Props(classOf[Controller], processingGroupCount, purgeInterval, ttl))
  val dispatcher = actorSystem.actorSelection("/user/messageDispatcher")

  implicit val secret = "everyBodyHasASecret"
  implicit val tokenExpirationInterval = 5000L
  implicit val bufferSize = 100
  implicit val objectMapper = new ObjectMapper
  objectMapper.registerModule(new DefaultScalaModule)

  implicit val defaultPatience = PatienceConfig(timeout = Span(2000, Seconds))

  val confidenceIntervalToPurge: Long = 1000

  val joinClientInterval = 10
  val toleranceInterval = 50

  val exec = Executors.newSingleThreadExecutor()

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    PublisherRegistry.clear()
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    controller ! PoisonPill
    dispatcher ! PoisonPill
    actorSystem.terminate().foreach { _ =>
      logger.info("Actor system was shut down")
    }
  }

  private[this] def timeoutTask(timeout: Timeout) = new Callable[Unit] {
    def call() = Thread.sleep(timeout.value.totalNanos / 1000000)
  }

  def waitUntilTimeout0(timeout: Timeout)(f: => Unit) = whenReady(exec.submit(timeoutTask(timeout))) { (Unit) => f }

  def waitUntilTimeout1[T1](timeout: Timeout)(v1: T1)(f: (T1) => Unit) = whenReady(exec.submit(timeoutTask(timeout))) { (Unit) => f(v1) }

  def waitUntilTimeout2[T1, T2](timeout: Timeout)(v1: T1, v2: T2)(f: (T1, T2) => Unit) = whenReady(exec.submit(timeoutTask(timeout))) { (Unit) => f(v1, v2) }

  def createTupleForLogin(subject: String, clientIdentificationKey: String, expiredToken: Boolean = false) = {
    val wsClient = WSProbe()(actorSystem, materializer)
    val token = TokenHandler.createTokenForSubject(subject, expiredToken)
    val path = "/rtm/" + token.right.get + "/" + clientIdentificationKey
    (wsClient, token, path)
  }

  def logout(wsClient: WSProbe, token: String, subject: String, clientIdentificationKey: String) = {
    wsClient.sendMessage(objectMapper.writeValueAsString(WebFlowSecurityData(token, clientIdentificationKey, logout = true)))
    eventually(interval(Span(toleranceInterval, Milliseconds)))(
      PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(subject, clientIdentificationKey) should be(None)
    )
  }
}
