package com.realtime.messaging.security

import com.fasterxml.jackson.databind.ObjectMapper
import com.realtime.messaging.common.exception.{FailReason, FailResult}
import com.realtime.messaging.common.security.TokenHandler
import com.realtime.messaging.model._
import com.typesafe.scalalogging.LazyLogging


object WebFlowSecurityManager extends LazyLogging {

  def transformWebFlowSecurityDataToConsumerData(token: String, clientIdentificationKey: String)(implicit secret: String, tokenExpirationInterval: Long): Either[FailResult, ConsumerData] = {
    try {
      require(token != null && clientIdentificationKey != null)
      TokenHandler.transformTokenToSubjectSecurityData(token) match {
        case Right(s) => Right(ConsumerData(s.uuid, clientIdentificationKey, s.expiresAt))
        case Left(f) => Left(f)
      }
    }
    catch {
      case ex: IllegalArgumentException => Left(FailResult(FailReason.UNAUTHENTICATED_CALL))
    }
  }

  def transformWebFlowInboundData(webFlowInboundDataAsJson: String, uuid: String, clientIdentificationKey: String)(implicit secret: String, tokenExpirationInterval: Long, objectMapper: ObjectMapper): Option[Protocol] = {
    try {
      require(webFlowInboundDataAsJson != null)
      webFlowInboundDataAsJson match {
        case msg if msg.contains("token") =>
          val webFlowSecurityData = objectMapper.readValue(webFlowInboundDataAsJson, classOf[WebFlowSecurityData])
          TokenHandler.transformTokenToSubjectSecurityData(webFlowSecurityData.token) match {
            case Right(s) =>
              assert(uuid == s.uuid)
              if (webFlowSecurityData.logout)
                Some(Logout(s.uuid, webFlowSecurityData.clientIdentificationKey))
              else
                Some(ResumeFlowData(ConsumerData(s.uuid, webFlowSecurityData.clientIdentificationKey, s.expiresAt)))
            case Left(f) => logger.warn("Failed to deserialize because of {}" + f.failReason); None
          }
        case msg if msg.contains("\"topics\"") => Some(Registration(uuid, clientIdentificationKey, objectMapper.readValue(webFlowInboundDataAsJson, classOf[Registration]).topics))
        case msg if msg.contains("\"topicIds\"") => Some(UnRegistration(uuid, clientIdentificationKey, objectMapper.readValue(webFlowInboundDataAsJson, classOf[UnRegistration]).topicIds))
        case _ => None
      }
    }
    catch {
      case _: Throwable => None
    }
  }
}
