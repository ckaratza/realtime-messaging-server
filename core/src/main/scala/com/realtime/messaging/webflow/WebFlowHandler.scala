package com.realtime.messaging.webflow

import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.stream.FlowShape
import akka.stream.scaladsl._
import com.fasterxml.jackson.databind.ObjectMapper
import com.realtime.messaging.model._
import com.realtime.messaging.security.WebFlowSecurityManager
import com.typesafe.scalalogging.LazyLogging

object WebFlowHandler extends LazyLogging {

  def webSocketFlow(controller: ActorRef, consumerData: ConsumerData)(implicit secret: String, tokenExpirationInterval: Long, bufferSize: Int, objectMapper: ObjectMapper): Flow[Message, Message, Any] = {

    val updateExpirationTime: PartialFunction[Protocol, Long] = {
      case data: ResumeFlowData => data.consumerData.expiresAt
    }

    Flow.fromGraph(GraphDSL.create() {
      implicit b =>
        import akka.stream.scaladsl.GraphDSL.Implicits._
        val uuid = consumerData.uuid
        val clientIdentificationKey = consumerData.clientIdentificationKey
        var expiresAt = consumerData.expiresAt
        val source = Source.actorPublisher[String](Props(classOf[Publisher], controller, consumerData.uuid, consumerData.clientIdentificationKey, consumerData.expiresAt, bufferSize))
        val merge = b.add(Merge[String](2))
        val filter = b.add(Flow[String].filter(s => false))
        val mapInboundMsgToString = b.add(Flow[Message].map[String] {
          case TextMessage.Strict(inbound) =>
            WebFlowSecurityManager.transformWebFlowInboundData(inbound, uuid, clientIdentificationKey) match  {
                case Some(protocol) =>
                if (updateExpirationTime.isDefinedAt(protocol)) expiresAt = updateExpirationTime(protocol)
                if (System.currentTimeMillis() > expiresAt)  {
                  controller ! UpdateSession(uuid, clientIdentificationKey)
                  ""
                }
                else {
                  controller ! protocol
                  ""
                }
                case None =>  Keep.none;logger.warn("Failed to deserialize {}", inbound);""
              }
          case x:Any => Keep.none;""
        })
        val mapStringToMsg = b.add(Flow[String].map[Message](x => TextMessage(x)))
        val serviceSource = b.add(source)
        mapInboundMsgToString ~> filter ~> merge
        serviceSource ~> merge ~> mapStringToMsg
        FlowShape(mapInboundMsgToString.in, mapStringToMsg.out)
    })
  }
}
