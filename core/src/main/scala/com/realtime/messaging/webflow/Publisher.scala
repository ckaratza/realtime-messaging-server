package com.realtime.messaging.webflow

import java.util.concurrent.ArrayBlockingQueue

import akka.actor.ActorRef
import akka.stream.actor.AbstractActorPublisher
import akka.stream.actor.ActorPublisherMessage.{Cancel, Request}
import com.realtime.messaging.model._
import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec

class Publisher(var router: ActorRef, uuid: String, clientIdentificationKey: String, var expiresAt: Long)(implicit bufferSize: Int = 100)
  extends AbstractActorPublisher[String] with LazyLogging {

  private[Publisher] case object QueueUpdated

  private[this] val queue = new ArrayBlockingQueue[String](bufferSize)
  private[this] var queueUpdated = false
  private[this] var allowFlow = true

  override def receive: Receive = {
    case msg: String =>
      logger.debug("Receiving message with payload {}", msg)
      add(msg)
      if (hasExpired) {
        self ! RefreshSession
      }
      else if (!queueUpdated) {
        queueUpdated = true
        self ! QueueUpdated
      }
    case RefreshSession =>
      if (allowFlow) {
        logger.warn("Session has expired for publisher with uuid {} and clientIdentificationKey {}.", uuid, clientIdentificationKey)
        allowFlow = false
        onNext(RefreshSession.refresh)
      }
    case x: ResumeFlowData =>
      logger.debug("Resuming flow for publisher with uuid {} and clientIdentificationKey {}.", uuid, clientIdentificationKey)
      allowFlow = true
      expiresAt = x.consumerData.expiresAt
      deliver()
    case Cancel => logger.debug("Stream terminated for publisher with uuid {} and clientIdentificationKey {}.", uuid, clientIdentificationKey); context.stop(self)
    case QueueUpdated => deliver()
    case logout: Logout => context.stop(self)
    case Request(x) => deliver()
    case x: Any => unhandled(x)
  }

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    logger.debug("Registering publisher with uuid {} and clientIdentificationKey {}", uuid, clientIdentificationKey)
    router ! ConsumerData(uuid, clientIdentificationKey, expiresAt)
  }


  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    logger.warn("Stopping publisher with uuid {} and clientIdentificationKey {}", uuid, clientIdentificationKey)
    router ! TerminationSignal(uuid, clientIdentificationKey)
    router = null
  }

  @tailrec
  private[this] def deliver(): Unit = {
    if (queue.size == 0 && totalDemand > 0) queueUpdated = false
    else if (allowFlow && queue.size > 0 && totalDemand > 0) {
      onNext(queue.poll())
      deliver()
    }
  }

  private[this] def hasExpired = expiresAt < System.currentTimeMillis

  private[this] def add(msg: String) = {
    if (msg != null && !msg.isEmpty) {
      if (queue.size == bufferSize) {
        logger.warn("Internal Buffer exceeded for publisher with uuid {} and clientIdentificationKey {}", uuid, clientIdentificationKey)
        queue.poll
      }
      queue.add(msg)
    }
  }
}