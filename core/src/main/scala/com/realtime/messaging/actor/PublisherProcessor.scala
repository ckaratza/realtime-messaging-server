package com.realtime.messaging.actor

import akka.actor.{Terminated, Actor, ActorRef}
import com.realtime.messaging.model.{Message, PurgeTopic, TopicRegistration}
import com.realtime.messaging.service.PublisherService

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

private[actor] case class Dispatch(id: String)

private[actor] class PublisherProcessor(val processingGroupCount: Int, val groupOrdinal: Int) extends Actor with PublisherService {

  private val hasDispatchBeenSentMap = mutable.HashMap[String, Boolean]()
  private val subscriberToTopicsMap = mutable.HashMap[ActorRef, ListBuffer[String]]()

  def dispatchAndMarkAsDispatched(id: String) = {
    if (!hasDispatchBeenSentMap.getOrElse(id, false)) {
      self.tell(new Dispatch(id), ActorRef.noSender)
      hasDispatchBeenSentMap.put(id, true)
    }
  }

  def addTopicForActor(actorRef: ActorRef, id: String) = {
    subscriberToTopicsMap.get(actorRef) match {
      case Some(list) => list.+=(id)
      case None =>
        val list = new ListBuffer[String]()
        list.+=(id)
        subscriberToTopicsMap.put(actorRef, list)
    }
  }

  override def receive: Receive = {
    case subscriberPresenceQuery: SubscriberPresenceQuery => updateSubscriberPresenceStatusForTopicGroup(subscriberPresenceQuery.topicIndex, groupOrdinal)
    case purgeTopic: PurgeTopic => hasDispatchBeenSentMap.remove(purgeTopic.id)
    case dispatch: Dispatch =>
      dispatchMessagesToTopicGroup(dispatch.id, groupOrdinal)
      hasDispatchBeenSentMap.put(dispatch.id, false)
    case msg: Message =>
      addMessagesToTopicGroup(msg.topicId.get, groupOrdinal, Array(msg))
      dispatchAndMarkAsDispatched(msg.topicId.get)
    case topicRegistration: TopicRegistration =>
      context.watch(sender())
      addPublisherToTopicGroup(topicRegistration.id, sender(), topicRegistration.fromOrdinal, groupOrdinal)
      addTopicForActor(sender(), topicRegistration.id)
      dispatchAndMarkAsDispatched(topicRegistration.id)
    case terminated: Terminated =>
      subscriberToTopicsMap.get(terminated.actor) match {
        case Some(list) => list.foreach(id => removePublisherFromTopicGroup(id, terminated.actor, groupOrdinal)); subscriberToTopicsMap.remove(terminated.actor)
        case None =>
      }
    case id: String => removePublisherFromTopicGroup(id, sender(), groupOrdinal)
    case x: Any => unhandled(x)
  }
}
