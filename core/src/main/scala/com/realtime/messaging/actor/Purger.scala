package com.realtime.messaging.actor

import akka.actor.{ActorRef, Actor}
import com.realtime.messaging.service.PurgeService

import scala.concurrent.duration._
import scala.language.postfixOps

case class SubscriberPresenceQuery(topicIndex: Int)

private[actor] class Purger(purgeInterval: Int, val publisherProcessors: Array[ActorRef]) extends Actor with PurgeService {

  private[Purger] val AskForStatus = "AsK"
  private[Purger] val TryToPurge = "TryToPurge"

  implicit val executor = context.system.dispatcher
  val statusPoller = context.system.scheduler.schedule((purgeInterval / 2) seconds, purgeInterval seconds, self, AskForStatus)
  val purger = context.system.scheduler.schedule(purgeInterval seconds, purgeInterval seconds, self, TryToPurge)

  override def receive: Receive = {
    case AskForStatus => askTopicSubscriberStatus(publisherProcessors)
    case TryToPurge => performTopicPurge(context.parent)
    case x: Any => unhandled(x)
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    statusPoller.cancel()
    purger.cancel()
  }
}
