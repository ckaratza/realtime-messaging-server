package com.realtime.messaging.actor

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{OneForOneStrategy, Actor, PoisonPill, Props}
import com.realtime.messaging.model._
import com.realtime.messaging.service.PublisherService
import com.typesafe.scalalogging.LazyLogging

class Controller(val processingGroupCount: Int, val purgeInterval: Int, val ttl: Int) extends Actor with LazyLogging with PublisherService {

  private[this] val publisherProcessors = Array.tabulate(processingGroupCount) { ordinal =>
    context.actorOf(Props(classOf[PublisherProcessor], processingGroupCount, ordinal).withDispatcher("pinned-dispatcher").withMailbox("priority-mailbox"), "pp" + ordinal)
  }

  private[this] val messageDispatcher = context.system.actorOf(Props(classOf[MessageDispatcher], publisherProcessors), "messageDispatcher")

  private[this] val purger = context.actorOf(Props(classOf[Purger], purgeInterval, publisherProcessors), "purger")

  override def receive: Receive = {
    case terminationSignal: TerminationSignal => terminatePublisher(terminationSignal.uuid, terminationSignal.clientIdentificationKey)
    case logout: Logout => logoutPublisher(logout)
    case updateSession: UpdateSession => signalPublisherToUpdateSession(updateSession)
    case consumerData: ConsumerData => joinPublisher(consumerData, sender())
    case resumeFlowData: ResumeFlowData => resumeFlowForPublisher(resumeFlowData)
    case registration: Registration => registerPublisherToTopics(registration, publisherProcessors, ttl)
    case unRegistration: UnRegistration => unRegisterPublisherFromTopics(unRegistration, publisherProcessors)
    case purgeTopic: PurgeTopic =>
      logger.debug("Purging topic {}", purgeTopic.id)
      removeTopic(purgeTopic.id)
      publisherProcessors.foreach(ref => ref ! purgeTopic)
    case PoisonPill =>
      messageDispatcher ! PoisonPill
      purger ! PoisonPill
      publisherProcessors.foreach(ref => ref ! PoisonPill)
    case x: Any => unhandled(x)
  }

  override val supervisorStrategy = OneForOneStrategy() {
    case _ => Resume
  }

}

