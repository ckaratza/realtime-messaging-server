package com.realtime.messaging.actor

import akka.actor.{Terminated, ActorSystem}
import akka.dispatch.{PriorityGenerator, UnboundedStablePriorityMailbox}
import com.realtime.messaging.model.{Message, TopicRegistration}
import com.typesafe.config.Config

private[actor] class PriorityMailBox(settings: ActorSystem.Settings, config: Config)
  extends UnboundedStablePriorityMailbox(
    PriorityGenerator {
      case id: String => 0
      case Terminated => 0
      case topicRegistration: TopicRegistration => 0
      case subscriberPresenceQuery: SubscriberPresenceQuery => 1
      case dispatch: Dispatch => 2
      case msg: Message => 3
      case _: Any => 4
    })
