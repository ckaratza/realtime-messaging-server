package com.realtime.messaging.actor

import akka.actor.{Actor, ActorRef}
import com.realtime.messaging.model.Message
import com.realtime.messaging.service.PublisherService
import com.typesafe.scalalogging.LazyLogging

private[actor] class MessageDispatcher(val publisherProcessors: Array[ActorRef]) extends Actor with PublisherService with LazyLogging {

  override val processingGroupCount: Int = publisherProcessors.length

  override def receive: Receive = {
    case msg: Message => logger.debug("Dispatching Message {}", msg); dispatchMessage(msg, publisherProcessors)
    case x: Any => unhandled(x)
  }

}
