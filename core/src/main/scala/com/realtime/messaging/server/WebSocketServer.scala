package com.realtime.messaging.server

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import com.fasterxml.jackson.databind.ObjectMapper
import com.realtime.messaging.security.WebFlowSecurityManager
import com.realtime.messaging.webflow.WebFlowHandler
import com.typesafe.scalalogging.LazyLogging

trait WebSocketServer extends LazyLogging {

  val controller: ActorRef
  implicit val secret: String
  implicit val tokenExpirationInterval: Long
  implicit val bufferSize: Int
  implicit val objectMapper: ObjectMapper

  val wsRoute =
    path("rtm" / Segment / Segment) {
      (token, clientIdentificationKey) => {
        WebFlowSecurityManager.transformWebFlowSecurityDataToConsumerData(token, clientIdentificationKey) match {
          case Right(consumerData) => get {
            handleWebSocketMessages(WebFlowHandler.webSocketFlow(controller, consumerData))
          }
          case Left(failResult) => logger.warn("Unauthenticated Attempt Recorded"); complete(StatusCodes.Unauthorized)
        }
      }
    }
}
