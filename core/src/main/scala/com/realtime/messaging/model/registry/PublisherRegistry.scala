package com.realtime.messaging.model.registry

import akka.actor.ActorRef

import scala.collection.mutable

private[model] case class PublisherData(actorRef: ActorRef, clientIdentificationKey: String, var expiresAt: Long)

object PublisherRegistry {

  private[PublisherRegistry] val publishers = new mutable.HashMap[String, mutable.Set[PublisherData]] with mutable.MultiMap[String, PublisherData]

  def clear() = publishers.clear()

  def size = publishers.size

  def findPublishersForUUID(uuid: String) = {
    require(uuid != null && !uuid.isEmpty)
    publishers.get(uuid)
  }

  def findPublisherForUuidAndClientIdentificationKey(uuid: String, clientIdentificationKey: String) = {
    require(uuid != null && !uuid.isEmpty && clientIdentificationKey != null && !clientIdentificationKey.isEmpty)
    findPublishersForUUID(uuid) match {
      case Some(set) if set.nonEmpty => set.find { entry => clientIdentificationKey == entry.clientIdentificationKey }
      case None => None
    }
  }

  def addNewPublisher(uuid: String, clientIdentificationKey: String, expiresAt: Long, actorRef: ActorRef) = {
    require(uuid != null && !uuid.isEmpty && clientIdentificationKey != null && !clientIdentificationKey.isEmpty && expiresAt > 0 && actorRef != null)
    removePublisher(uuid, clientIdentificationKey)
    publishers.addBinding(uuid, PublisherData(actorRef, clientIdentificationKey, expiresAt))
  }

  def removePublisher(uuid: String, clientIdentificationKey: String) = {
    require(uuid != null && !uuid.isEmpty && clientIdentificationKey != null && !clientIdentificationKey.isEmpty)
    findPublisherForUuidAndClientIdentificationKey(uuid, clientIdentificationKey) match {
      case Some(publisherData) => publishers.removeBinding(uuid, publisherData)
      case None => None
    }
  }

  def refreshExpirationTimeForUuidAndClientIdentificationKey(uuid: String, clientIdentificationKey: String, expiresAt: Long) = {
    require(uuid != null && !uuid.isEmpty && clientIdentificationKey != null && !clientIdentificationKey.isEmpty && expiresAt > 0)
    findPublisherForUuidAndClientIdentificationKey(uuid, clientIdentificationKey) match {
      case Some(publisherData) => publisherData.expiresAt = expiresAt
      case None =>
    }
  }
}
