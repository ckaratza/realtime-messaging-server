package com.realtime.messaging.model

sealed abstract class Protocol

sealed abstract class Inbound extends Protocol

sealed abstract class Outbound extends Protocol

case class Message(ordinal: Long, payload: String, topicId: Option[String], recipientList: Option[Array[String]] = None, var timeStamp: Long = 0) extends Outbound

case object RefreshSession extends Outbound {
  val refresh = "{\"cmd\":\"refresh\"}"
}

case class PurgeTopic(id: String)

case class TopicRegistration(id: String, fromOrdinal: Long = 0)

case class Registration(uuid: String, clientIdentificationKey: String, topics: Array[TopicRegistration]) extends Inbound

case class UnRegistration(uuid: String, clientIdentificationKey: String, topicIds: Array[String]) extends Inbound

case class WebFlowSecurityData(token: String, clientIdentificationKey: String, logout: Boolean = false) extends Inbound

case class ConsumerData(uuid: String, clientIdentificationKey: String, expiresAt: Long) extends Protocol

case class Logout(uuid: String, clientIdentificationKey: String) extends Protocol

case class ResumeFlowData(consumerData: ConsumerData) extends Protocol

case class UpdateSession(uuid: String, clientIdentificationKey: String) extends Protocol

case class TerminationSignal(uuid: String, clientIdentificationKey: String) extends Protocol