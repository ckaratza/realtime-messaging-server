package com.realtime.messaging.model.registry

import java.util
import java.util.Comparator
import java.util.concurrent.atomic.AtomicLong

import akka.actor.ActorRef
import com.carrotsearch.hppc.{ObjectArrayList, ObjectIntHashMap, ObjectIntMap}
import com.realtime.messaging.model.Message
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable

private[registry] class Topic(val processingGroupsCount: Int, var id: String = null, var available: Boolean = true, var lastSubscriberRegistrationTime: Long = -1) {

  val processingGroups = Array.fill(processingGroupsCount) {
    new TopicProcessingGroup
  }

  def assign(id: String, ttl: Int) = {
    this.id = id
    lastSubscriberRegistrationTime = System.currentTimeMillis()
    available = false
    resetWithTTL(ttl)
  }

  def unAssign() = available = true

  def assignSubscriberToGroup(actorRef: ActorRef, sendMessagesFromId: Long, groupOrdinal: Int) = processingGroups(groupOrdinal).addPublisher(actorRef, sendMessagesFromId)

  def unAssignSubscriberFromGroup(actorRef: ActorRef, groupOrdinal: Int) = processingGroups(groupOrdinal).removePublisher(actorRef)

  def dispatchMessagesForProcessingGroupOfTopic(groupOrdinal: Int) = processingGroups(groupOrdinal).dispatchMessagesToPublishers()

  def addMessagesToProcessingGroup(groupOrdinal: Int, messages: Array[Message]) = processingGroups(groupOrdinal).addMessages(messages)

  def doesProcessingGroupOfTopicHaveSubscribers(groupOrdinal: Int) = processingGroups(groupOrdinal).hasSubscribers

  private[this] def resetWithTTL(ttl: Int) = processingGroups.foreach(processingGroup => processingGroup.resetWithTTL(ttl))
}

private[registry] case class Publisher(actorRef: ActorRef, var fromOrdinal: Long) {
  override def equals(obj: Any) = {
    this.actorRef == obj.asInstanceOf[Publisher].actorRef
  }
}

private[registry] class TopicProcessingGroup extends LazyLogging {

  val monitorRegistrationSet = new mutable.HashSet[ActorRef]()
  val processingArea = new ObjectArrayList[Publisher]
  val content = new TopicContent()

  def hasSubscribers: Boolean = processingArea.size() > 0

  def resetWithTTL(ttl: Int) = {
    processingArea.clear()
    content.clear()
    content.ttl = ttl
  }

  def addPublisher(actorRef: ActorRef, sendMessagesFromOrdinal: Long) = {
    monitorRegistrationSet.apply(actorRef) match {
      case false => monitorRegistrationSet.add(actorRef); processingArea.add(Publisher(actorRef, sendMessagesFromOrdinal)); content.calculateMaximumPreserveId(sendMessagesFromOrdinal)
      case true => logger.warn("Attempt to register publisher again")
    }
  }

  def removePublisher(actorRef: ActorRef) = {
    monitorRegistrationSet.remove(actorRef) match {
      case true => processingArea.removeFirst(Publisher(actorRef, 0));
      case false => logger.warn("No publisher to remove")
    }
  }

  private[this] def processPublisher(publisher: Publisher, messages: ObjectArrayList[Message]) = {
    for (i <- 0 until messages.size) {
      if (publisher.fromOrdinal <= messages.get(i).ordinal) {
        publisher.actorRef.tell(messages.get(i).payload, ActorRef.noSender)
        publisher.fromOrdinal = publisher.fromOrdinal + 1
      }
    }
    if (messages.size() > 0) {
      val newFromOrdinal = messages.get(messages.size - 1).ordinal
      if (publisher.fromOrdinal < newFromOrdinal) {
        publisher.fromOrdinal = newFromOrdinal
        content.calculateMaximumPreserveId(newFromOrdinal + 1)
      }
    }
  }

  def dispatchMessagesToPublishers() = {
    content.getNextMessageBatchForDelivery match {
      case Some(messages) =>
        val iterator = processingArea.iterator
        while (iterator.hasNext) processPublisher(iterator.next.value, messages)
      case None =>
    }
  }

  def addMessages(messages: Array[Message]) = {
    content.addMessages(messages)
  }
}

private[registry] object TopicContent {
  private val ordering = new Comparator[Message] {
    override def compare(x: Message, y: Message): Int = {
      (x.ordinal - y.ordinal).toInt
    }
  }
}

private[registry] class TopicContent(var ttl: Int = 2000) {

  var maximumPreserveId: Long = Long.MaxValue
  val orderedSet = new util.TreeSet[Message](TopicContent.ordering)

  private[this] def hasMessageExpired(msg: Message): Boolean = {
    msg.timeStamp + ttl < System.currentTimeMillis()
  }

  def addMessages(messages: Array[Message]) {
    messages.foreach(message => {
      require(message.payload != null && !message.payload.isEmpty)
      message.timeStamp = System.currentTimeMillis()
      orderedSet.add(message)
    })
  }

  def getNextMessageBatchForDelivery: Option[ObjectArrayList[Message]] = {
    if (!orderedSet.isEmpty) {
      val messages = new ObjectArrayList[Message]
      val it = orderedSet.iterator
      while (it.hasNext) {
        val msg = it.next
        if ((maximumPreserveId != Long.MaxValue && msg.ordinal < maximumPreserveId) || hasMessageExpired(msg)) it.remove()
        else messages.add(msg)
      }
      return Some(messages)
    }
    None
  }

  def calculateMaximumPreserveId(input: Long) {
    maximumPreserveId = if (maximumPreserveId > input) input else maximumPreserveId
  }

  def clear() = {
    orderedSet.clear()
    this.maximumPreserveId = Long.MaxValue
  }
}

object TopicRegistry extends LazyLogging {

  private[TopicRegistry] val freeSlotStack = mutable.Stack[Int]()
  private[TopicRegistry] var topics: ObjectArrayList[Topic] = new ObjectArrayList(0)
  private[TopicRegistry] var topicsSubscriberStates: ObjectArrayList[TopicSubscriberState] = new ObjectArrayList(0)
  private[TopicRegistry] val indexCache: ObjectIntMap[String] = new ObjectIntHashMap[String]
  private[TopicRegistry] var processingGroupCount: Int = 4
  private[TopicRegistry] var confidenceIntervalToPurge: Long = 1000

  private[TopicRegistry] case class TopicSubscriberState(processingGroupCount: Int) {

    private[TopicRegistry] case class Entry(var forQuery: Long = -1, var hasSubscribers: Boolean = true)

    val state = Array.fill(processingGroupCount) {
      Entry()
    }

    def reset() = {
      state.foreach(entry => {
        entry.forQuery = -1
        entry.hasSubscribers = true
      })
    }

    def hasSubscribers: Boolean = {
      val queryToCompare = state(0).forQuery
      state.count(entry => (entry.forQuery != queryToCompare) || entry.hasSubscribers) > 0
    }

    def updateEntry(index: Int, value: Boolean) = {
      state(index).hasSubscribers = value
      state(index).forQuery = QueryIdGenerator.getQueryId
    }
  }

  def buildTopicStore(universeSize: Int, processingGroupCount: Int, confidenceIntervalToPurge: Long) = {
    require(universeSize > 0 && processingGroupCount > 0)
    freeSlotStack.clear()
    topics = new ObjectArrayList[Topic](universeSize)
    topicsSubscriberStates = new ObjectArrayList[TopicSubscriberState](universeSize)
    for (i <- 0 until universeSize) {
      topics.add(new Topic(processingGroupCount))
      topicsSubscriberStates.add(new TopicSubscriberState(processingGroupCount))
      this.processingGroupCount = processingGroupCount
      this.confidenceIntervalToPurge = confidenceIntervalToPurge
      freeSlotStack.push(i)
    }
  }

  @throws[NoSuchElementException]
  def addTopic(id: String, ttl: Int): Boolean = {
    require(id != null)
    findOperationalTopic(id) match {
      case Some(topic) =>
        topic.lastSubscriberRegistrationTime = System.currentTimeMillis()
        false
      case None =>
        val freeSlot = freeSlotStack.pop()
        topics.get(freeSlot).assign(id, ttl)
        indexCache.put(id, freeSlot)
        true
    }
  }

  def universeSize = topics.size()

  def freeTopicsSize = freeSlotStack.size

  def removeTopic(id: String) = {
    require(id != null)
    indexCache.getOrDefault(id, -1) match {
      case -1 => None
      case index: Int =>
        topics.get(index).unAssign()
        topicsSubscriberStates.get(index).reset()
        indexCache.remove(id)
        freeSlotStack.push(index)
    }
  }

  def decideIfTopicShouldBePurged(topicIndex: Int): Option[String] = {
    val topic = topics.get(topicIndex)
    topic.available match {
      case false =>
        if (!topicsSubscriberStates.get(topicIndex).hasSubscribers) {
          if (System.currentTimeMillis() - topic.lastSubscriberRegistrationTime > confidenceIntervalToPurge)
            Some(topic.id)
          else None
        }
        else None
      case true => None
    }
  }

  def updateSubscriberPresenceStatusAndReturnStatusForTopicGroup(topicIndex: Int, groupOrdinal: Int) = {
    val topic = topics.get(topicIndex)
    topic.available match {
      case false =>
        val groupHasSubscribers = topic.doesProcessingGroupOfTopicHaveSubscribers(groupOrdinal)
        topicsSubscriberStates.get(topicIndex).updateEntry(groupOrdinal, groupHasSubscribers)
        logger.debug("Receiving update subscriber status for {}, groupOrdinal {}. Group has subscribers->{}", topicIndex + "", groupOrdinal + "", groupHasSubscribers + "")
      case true =>
    }
  }

  def addPublisherToProcessingGroupOfTopic(id: String, actorRef: ActorRef, fromOrdinal: Long, groupOrdinal: Int) {
    findOperationalTopic(id) match {
      case Some(topic) => topic.assignSubscriberToGroup(actorRef, fromOrdinal, groupOrdinal)
      case None =>
    }
  }

  def removePublisherFromProcessingGroupOfTopic(id: String, actorRef: ActorRef, groupOrdinal: Int) {
    findOperationalTopic(id) match {
      case Some(topic) => topic.unAssignSubscriberFromGroup(actorRef, groupOrdinal)
      case None =>
    }
  }

  def addMessagesToProcessingGroupOfTopic(id: String, groupOrdinal: Int, messages: Array[Message]) {
    findOperationalTopic(id) match {
      case Some(topic) => topic.addMessagesToProcessingGroup(groupOrdinal, messages)
      case None =>
    }
  }

  def dispatchMessagesForProcessingGroupOfTopic(id: String, groupOrdinal: Int) {
    findOperationalTopic(id) match {
      case Some(topic) => topic.dispatchMessagesForProcessingGroupOfTopic(groupOrdinal)
      case None =>
    }
  }

  private[TopicRegistry] def findOperationalTopic(id: String): Option[Topic] = {
    indexCache.getOrDefault(id, -1) match {
      case -1 => None
      case index: Int =>
        if (!topics.get(index).available)
          Some(topics.get(index))
        else None
    }
  }
}

object QueryIdGenerator {
  private val queryId: AtomicLong = new AtomicLong(0)

  def moveToNextQueryId() = queryId.incrementAndGet()

  def getQueryId = queryId.get
}
