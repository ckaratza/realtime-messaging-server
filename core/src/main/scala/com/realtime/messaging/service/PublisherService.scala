package com.realtime.messaging.service

import akka.actor.{Actor, ActorRef}
import com.realtime.messaging.model._
import com.realtime.messaging.model.registry.{TopicRegistry, PublisherRegistry}
import com.typesafe.scalalogging.LazyLogging

trait PublisherService extends PartitioningService with LazyLogging {
  this: Actor =>

  val processingGroupCount: Int

  def logoutPublisher(logout: Logout) = {
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(logout.uuid, logout.clientIdentificationKey) match {
      case Some(publisherData) => publisherData.actorRef ! logout
      case None => logger.debug("Publisher with uuid {} and clientIdentificationKey {} not found.", logout.uuid, logout.clientIdentificationKey)
    }
  }

  def signalPublisherToUpdateSession(updateSession: UpdateSession) = {
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(updateSession.uuid, updateSession.clientIdentificationKey) match {
      case Some(publisherData) => publisherData.actorRef ! RefreshSession
      case None => logger.debug("Publisher with uuid {} and clientIdentificationKey {} not found.", updateSession.uuid, updateSession.clientIdentificationKey)
    }
  }

  def terminatePublisher(uuid: String, clientIdentificationKey: String) = {
    logger.debug("Removing publisher with uuid {} and clientIdentificationKey {}", uuid, clientIdentificationKey)
    PublisherRegistry.removePublisher(uuid, clientIdentificationKey)
  }

  def joinPublisher(consumerData: ConsumerData, actorRef: ActorRef) = {
    logger.debug("Joining publisher with uuid {} and clientIdentificationKey {} and actorRef {}", consumerData.uuid, consumerData.clientIdentificationKey, actorRef)
    PublisherRegistry.addNewPublisher(consumerData.uuid, consumerData.clientIdentificationKey, consumerData.expiresAt, actorRef)
  }

  def resumeFlowForPublisher(resumeFlowData: ResumeFlowData) = {
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(resumeFlowData.consumerData.uuid, resumeFlowData.consumerData.clientIdentificationKey) match {
      case Some(publisherData) => publisherData.actorRef ! resumeFlowData
      case None => logger.warn("Publisher with uuid {} and clientIdentificationKey {} not found.", resumeFlowData.consumerData.uuid, resumeFlowData.consumerData.clientIdentificationKey)
    }
  }

  def registerPublisherToTopics(registration: Registration, publisherProcessors: Array[ActorRef], ttl: Int) = {
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(registration.uuid, registration.clientIdentificationKey) match {
      case Some(publisherData) =>
        val publisherProcessor = publisherProcessors(resolvePartitionForSubscription(registration.uuid, processingGroupCount))
        registration.topics.filter(topic => topic != null && topic.id != null && !topic.id.isEmpty).foreach(topic => {
          TopicRegistry.addTopic(topic.id, ttl)
          publisherProcessor.tell(topic, publisherData.actorRef)
        })
      case None => logger.warn("No publisher found for publisher with uuid {} and clientIdentificationKey {}", registration.uuid, registration.clientIdentificationKey)
    }
  }

  def unRegisterPublisherFromTopics(unRegistration: UnRegistration, publisherProcessors: Array[ActorRef]) = {
    PublisherRegistry.findPublisherForUuidAndClientIdentificationKey(unRegistration.uuid, unRegistration.clientIdentificationKey) match {
      case Some(publisherData) =>
        val publisherProcessor = publisherProcessors(resolvePartitionForSubscription(unRegistration.uuid, processingGroupCount))
        unRegistration.topicIds.filter(topicId => topicId != null && !topicId.isEmpty).foreach(id => {
          publisherProcessor.tell(id, publisherData.actorRef)
        })
      case None => logger.warn("No publisher found for publisher with uuid {} and clientIdentificationKey {}", unRegistration.uuid, unRegistration.clientIdentificationKey)
    }
  }

  def addMessagesToTopicGroup(id: String, groupOrdinal: Int, msgs: Array[Message]) = TopicRegistry.addMessagesToProcessingGroupOfTopic(id, groupOrdinal, msgs.filter(message => message != null && message.payload != null && !message.payload.isEmpty))

  def addPublisherToTopicGroup(id: String, actorRef: ActorRef, fromOrdinal: Long, groupOrdinal: Int) = TopicRegistry.addPublisherToProcessingGroupOfTopic(id, actorRef, fromOrdinal, groupOrdinal)

  def removePublisherFromTopicGroup(id: String, actorRef: ActorRef, groupOrdinal: Int) = TopicRegistry.removePublisherFromProcessingGroupOfTopic(id, actorRef, groupOrdinal)

  def dispatchMessagesToTopicGroup(id: String, groupOrdinal: Int) = TopicRegistry.dispatchMessagesForProcessingGroupOfTopic(id, groupOrdinal)

  def removeTopic(id: String) = TopicRegistry.removeTopic(id)

  def updateSubscriberPresenceStatusForTopicGroup(topicIndex: Int, groupOrdinal: Int) = {
    TopicRegistry.updateSubscriberPresenceStatusAndReturnStatusForTopicGroup(topicIndex, groupOrdinal)
  }

  def dispatchMessage(msg: Message, publisherProcessors: Array[ActorRef]) = {
    msg.recipientList match {
      case Some(list) => dispatchPrivateMessageToRecipients(msg, list)
      case None => if (msg.topicId.isDefined) dispatchTopicEvent(msg, publisherProcessors)
    }
  }

  private[this] def dispatchPrivateMessageToRecipients(msg: Message, recipientList: Array[String]) = {
    recipientList.foreach(recipient => {
      PublisherRegistry.findPublishersForUUID(recipient) match {
        case Some(set) =>
          set.foreach(publisherData => {
            publisherData.actorRef ! msg.payload
          })
        case None => logger.warn("No publisher found for recipient with uuid {}", recipient)
      }
    })
  }

  private[this] def dispatchTopicEvent(msg: Message, publisherProcessors: Array[ActorRef]) = {
    publisherProcessors.foreach(actorRef => actorRef.tell(msg, ActorRef.noSender))
  }
}
