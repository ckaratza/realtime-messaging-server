package com.realtime.messaging.service


import akka.actor.{Actor, ActorRef}
import com.realtime.messaging.actor.SubscriberPresenceQuery
import com.realtime.messaging.model.PurgeTopic
import com.realtime.messaging.model.registry.{PublisherRegistry, QueryIdGenerator, TopicRegistry}
import com.typesafe.scalalogging.LazyLogging

trait PurgeService extends LazyLogging {
  this: Actor =>


  def performTopicPurge(controller: ActorRef) = {
    logger.debug("Attempt topic purging")
    for (i <- 0 until TopicRegistry.universeSize) {
      TopicRegistry.decideIfTopicShouldBePurged(i) match {
        case Some(id) => controller ! PurgeTopic(id)
        case None =>
      }
    }
  }

  def askTopicSubscriberStatus(publisherProcessors: Array[ActorRef]) = {
    QueryIdGenerator.moveToNextQueryId()
    logger.debug("Ask topics for subscriber status for {} subscribers", PublisherRegistry.size)
    for (i <- 0 until TopicRegistry.universeSize) {
      publisherProcessors.foreach(ref => ref ! SubscriberPresenceQuery(i))
    }
  }
}

