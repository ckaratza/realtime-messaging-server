package com.realtime.messaging.service

trait PartitioningService {

  def resolvePartitionForSubscription(uuid: String, groupPartitionCount: Int): Int = {
    Math.abs(uuid.hashCode % groupPartitionCount)
  }
}
