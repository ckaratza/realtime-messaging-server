package com.realtime.messaging.common.exception

object FailReason extends Enumeration {
  val UNAUTHENTICATED_CALL, SESSION_EXPIRATION, APPLICATION_CODE_ERROR, VALIDATION_ERROR = Value
}

case class FailResult(failReason: FailReason.Value)