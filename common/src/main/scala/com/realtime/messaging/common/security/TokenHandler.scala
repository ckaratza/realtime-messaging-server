package com.realtime.messaging.common.security

import java.util.Date

import com.realtime.messaging.common.exception.{FailReason, FailResult}
import io.jsonwebtoken.{ExpiredJwtException, Jwts, SignatureAlgorithm}

case class SubjectSecurityData(uuid: String, expiresAt: Long)

object TokenHandler {

  def transformTokenToSubjectSecurityData(token: String)(implicit secret: String, tokenExpirationInterval: Long): Either[FailResult, SubjectSecurityData] = {
    try {
      require(token != null && secret != null && tokenExpirationInterval > 0)
      val body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody
      Right(SubjectSecurityData(body.getSubject, body.getExpiration.getTime))
    }
    catch {
      case ex: ExpiredJwtException => Left(FailResult(FailReason.SESSION_EXPIRATION))
      case _: Throwable => Left(FailResult(FailReason.UNAUTHENTICATED_CALL))
    }
  }

  def createTokenForSubject(uuid: String, createExpiredToken: Boolean = false)(implicit secret: String, tokenExpirationInterval: Long): Either[FailResult, String] = {
    try {
      require(uuid != null && secret != null && tokenExpirationInterval > 0)
      Right(Jwts.builder().setExpiration(if (createExpiredToken) new Date(0) else new Date(System.currentTimeMillis + tokenExpirationInterval)).setSubject(uuid).signWith(SignatureAlgorithm.HS512, secret).compact())
    }
    catch {
      case ex: IllegalArgumentException => Left(FailResult(FailReason.VALIDATION_ERROR))
      case _: Throwable => Left(FailResult(FailReason.APPLICATION_CODE_ERROR))
    }
  }
}