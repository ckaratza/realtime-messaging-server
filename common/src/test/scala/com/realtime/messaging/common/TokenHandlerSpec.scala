package com.realtime.messaging.common

import com.realtime.messaging.common.exception.FailReason
import com.realtime.messaging.common.security.TokenHandler
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Milliseconds, Span}
import org.scalatest.{FlatSpec, Matchers}

class TokenHandlerSpec extends FlatSpec with Matchers with Eventually {

  implicit val secret = "everyBodyHasASecret"
  implicit val tokenExpirationInterval = 3000L
  val subjectFixture = "ckaratza"
  val badTokenFixture = "badToken"

  "A valid JWT producer and parser" should "properly create and then parse a JWT for subject " + subjectFixture in {
    val tokenDisjointUnion = TokenHandler.createTokenForSubject(subjectFixture)
    tokenDisjointUnion.right.get should not be null

    val subjectDisjointUnion = TokenHandler.transformTokenToSubjectSecurityData(tokenDisjointUnion.right.get)
    subjectDisjointUnion.right.get.uuid should be(subjectFixture)
  }

  "A valid JWT parser" should "reject a badly formed token" in {
    val subjectDisjointUnion = TokenHandler.transformTokenToSubjectSecurityData(badTokenFixture)
    subjectDisjointUnion.left.get.failReason should be(FailReason.UNAUTHENTICATED_CALL)
  }

  "A valid JWT parser" should "properly detect that a JWT created before the configured session timeout " + tokenExpirationInterval + "ms has expired" in {
    val expiredToken = TokenHandler.createTokenForSubject(subjectFixture).right.get
    eventually(timeout(Span(tokenExpirationInterval, Milliseconds))) {
      val subjectDisjointUnion = TokenHandler.transformTokenToSubjectSecurityData(expiredToken)
      subjectDisjointUnion.left.get.failReason should be(FailReason.SESSION_EXPIRATION)
    }
  }

  "A valid JWT parser" should "properly create an expired token for subject " + subjectFixture + " when requested" in {
    val tokenDisjointUnion = TokenHandler.createTokenForSubject(subjectFixture, createExpiredToken = true)
    val subjectDisjointUnion = TokenHandler.transformTokenToSubjectSecurityData(tokenDisjointUnion.right.get)
    subjectDisjointUnion.left.get.failReason should be(FailReason.SESSION_EXPIRATION)
  }
}
